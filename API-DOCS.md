![Condor Store](./condor-store.png)

# Condor Store API Documentation

## Test Resource
The mapping of the URI path space is presented in the following table:

URI Path | Resource class | HTTP methods
---------|----------------|-------------
/greeting|GreetingResource|GET

This application is configured using a java class, which registers 
`javax.ws.rs.core.Application` descendant to get classes and singletons 
from it (see class `CondorLabsRestConfig`).

## Category Resource
The mapping of the URI path space is presented in the following table:

URI Path | Resource class | HTTP methods
---------|----------------|-------------
/categories|CategoryResource|GET
/categories|CategoryResource|POST
/categories/:id|CategoryResource|DELETE

## Product Resource
The mapping of the URI path space is presented in the following table:

URI Path | Resource class | HTTP methods
---------|----------------|-------------
/products|ProductResource|GET
/products|ProductResource|POST
/products/:id|ProductResource|GET
/products/:id|ProductResource|PUT
/products/:id|ProductResource|DELETE
