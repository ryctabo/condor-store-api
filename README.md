![Condor Store](./condor-store.png)

# Condor Store API

[![pipeline status](https://gitlab.com/ryctabo/condor-store-api/badges/master/pipeline.svg)](https://gitlab.com/ryctabo/condor-store-api/commits/master)
[![coverage report](https://gitlab.com/ryctabo/condor-store-api/badges/master/coverage.svg)](https://gitlab.com/ryctabo/condor-store-api/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

This project is a API of Condor Labs's technical test.

## Getting Started

To start, you need to have installed Java version 8 and MySQL on your 
local machine.

## Development time

The project use [Gradle](https://gradle.org/) as a manager and to automate 
tasks. Use `build` gradle task to download local dependencies and compile 
the project and use `clean` to clean it.

Run the following command to clean and compile the project:

```
./gradlew clean build
```

### Database

We are using MySQL as a database management system, this project works with
the following parameters:

```text
user: condorlabs
password: condorlabs
database: condorlabs_store
```

Run the following SQL to create database and create user.

```sql
-- Create the database
CREATE DATABASE condorlabs_store;

-- Create the user in the database
CREATE USER condorlabs@localhost IDENTIFIED BY 'condorlabs';

-- Add all privileges of database tables to an user
GRANT ALL PRIVILEGES ON condorlabs_store.* TO condorlabs@localhost;
```

### Running server

This project uses [Gretty](http://akhikhl.github.io/gretty-doc/) to mount the API
and be able to make the corresponding requests locally.

```
./gradlew appRun
```

After the server is running, you can access the project through the browser
in the next url (http://localhost:8080/api.condorstore).

### Running the tests

The tests in this project are make with JUnit. You need use the following maven 
task:

```
./gradlew test
```

## Deployment

Just build the project you can get the packaged file (*.war) in the folder 
`/build/libs`, with the name of the project and the corresponding version.

But if in some case you are not able to run the following command and generate
the file.

```
./gradlew war
```

All development rights are reserved by [Gustavo Pacheco](https://ryctabo.wordpress.com).
