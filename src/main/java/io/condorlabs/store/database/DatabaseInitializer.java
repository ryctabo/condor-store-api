/*
 * The MIT License
 *
 * Copyright (c) 2019 Gustavo Pacheco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package io.condorlabs.store.database;

import io.condorlabs.store.database.entity.Category;
import io.condorlabs.store.database.entity.Product;
import io.condorlabs.store.database.repository.CategoryDao;
import io.condorlabs.store.database.repository.ProductDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
@Component
public class DatabaseInitializer {

    /**
     * Logger used to record all events in this class.
     */
    private static final Logger LOG = LogManager.getLogger(DatabaseInitializer.class);

    /**
     * The category repository.
     */
    private final CategoryDao categoryDao;

    /**
     * The product repository.
     */
    private final ProductDao productDao;

    /**
     * Create an instance of {@link DatabaseInitializer}, it's injecting
     * dependencies.
     *
     * @param categoryDao category repository
     * @param productDao  product repository
     */
    @Inject
    public DatabaseInitializer(CategoryDao categoryDao, ProductDao productDao) {
        this.categoryDao = categoryDao;
        this.productDao = productDao;
    }

    /**
     * Initial configuration of the database.
     */
    @PostConstruct
    private void setUp() {
        this.createDefaultCategories();
        this.createProducts();
    }

    /**
     * This method is used to create default products.
     */
    private void createProducts() {
        Product motorola1 = new Product();
        motorola1.setName("Motorola One");
        motorola1.setDescription("Big screen entertainment. Stunning portrait shots and selfies. " +
                "All-day battery plus turbopower™ charging. And the latest upgrades, innovations, " +
                "and experiences from motorola and Google. So you’re always ready.");
        motorola1.setPrice(399.99f);
        motorola1.setCategory(new Category(1L));
        motorola1.setCreated(LocalDateTime.now().minus(5, ChronoUnit.MINUTES));
        motorola1 = this.productDao.save(motorola1);
        motorola1.setUpdated(LocalDateTime.now());
        this.productDao.save(motorola1);

        Product motorola2 = new Product();
        motorola2.setName("Motorola G7");
        motorola2.setDescription("See it. capture it. interact with it.\n" +
                "Capture creative portraits and outdoor scenes with depth effects on a 12 MP dual camera system. " +
                "Search what you see with Google Lens™. Plus, enjoy an ultrawide 6.2\" Max Vision Full HD+ display.");
        motorola2.setPrice(299.99f);
        motorola2.setCategory(new Category(1L));
        motorola2.setCreated(LocalDateTime.now().minusMinutes(3));
        motorola2 = this.productDao.save(motorola2);
        motorola2.setUpdated(LocalDateTime.now().plusMinutes(2));
        this.productDao.save(motorola2);

        Product motorola3 = new Product();
        motorola3.setName("Motorola Z3");
        motorola3.setDescription("With the new moto z3, be the first to upgrade to Verizon’s 5G " +
                "network just by snapping on a moto mod™. Plus, get an all-day battery, " +
                "dual depth-sensing smart cameras, and more.");
        motorola3.setPrice(480.00f);
        motorola3.setCategory(new Category(1L));
        motorola3.setCreated(LocalDateTime.now().minusMinutes(2));
        motorola3 = this.productDao.save(motorola3);
        motorola3.setUpdated(LocalDateTime.now().minusMinutes(1));
        this.productDao.save(motorola3);

        Product motorola4 = new Product();
        motorola4.setName("Motorola G6");
        motorola4.setDescription("Meet moto g⁶. With a 5.7\" Full HD+ Max Vision display, advanced " +
                "imaging software, and a long-lasting battery, it’s impressive any way you look at it.");
        motorola4.setPrice(179.99f);
        motorola4.setCategory(new Category(1L));
        motorola4.setCreated(LocalDateTime.now().minusMinutes(1));
        motorola4 = this.productDao.save(motorola4);
        motorola4.setUpdated(LocalDateTime.now());
        this.productDao.save(motorola4);

        Product motorola5 = new Product();
        motorola5.setName("Motorola Z3 Play");
        motorola5.setDescription("With moto z3 play, go all weekend with up to 40 hours of " +
                "combined battery life just by snapping on a moto mod™. Plus, get a virtually " +
                "borderless display, dual depth-sensing cameras, and more.");
        motorola5.setPrice(499.99f);
        motorola5.setCategory(new Category(1L));
        motorola5.setCreated(LocalDateTime.now().minusMinutes(5));
        motorola5 = this.productDao.save(motorola5);
        motorola5.setUpdated(LocalDateTime.now().minusMinutes(1));
        this.productDao.save(motorola5);

        Product huawei1 = new Product();
        huawei1.setName("HUAWEI P30 Pro");
        huawei1.setPrice(1250.00f);
        huawei1.setCategory(new Category(1L));
        huawei1.setCreated(LocalDateTime.now().minus(2, ChronoUnit.MINUTES));
        this.productDao.save(huawei1);

        Product huawei2 = new Product();
        huawei2.setName("HUAWEI P30");
        huawei2.setPrice(899.99f);
        huawei2.setCategory(new Category(1L));
        huawei2.setCreated(LocalDateTime.now().minus(3, ChronoUnit.MINUTES));
        this.productDao.save(huawei2);

        Product samsung1 = new Product();
        samsung1.setName("Samsung Galaxy S10+");
        samsung1.setDescription("Store more, do more, get more.\n" +
                "Grab the Galaxy that’s right for you and get these exclusive deals.");
        samsung1.setPrice(1399.99f);
        samsung1.setCategory(new Category(1L));
        samsung1.setCreated(LocalDateTime.now().minus(3, ChronoUnit.MINUTES));
        this.productDao.save(samsung1);

        Product samsung2 = new Product();
        samsung2.setName("Samsung Galaxy S10");
        samsung2.setDescription("Store more, do more, get more.\n" +
                "Grab the Galaxy that’s right for you and get these exclusive deals.");
        samsung2.setPrice(999.99f);
        samsung2.setCategory(new Category(1L));
        samsung2.setCreated(LocalDateTime.now().minus(3, ChronoUnit.MINUTES));
        this.productDao.save(samsung2);

        Product product2 = new Product();
        product2.setName("iPhone XS");
        product2.setDescription("Super Retina in two sizes — including the largest display ever " +
                "on an iPhone. Even faster Face ID. The smartest, most powerful chip in a smartphone. " +
                "And a breakthrough dual-camera system with Depth Control. iPhone XS is everything " +
                "you love about iPhone. Taken to the extreme.");
        product2.setPrice(1689.99f);
        product2.setCategory(new Category(2L));
        product2.setCreated(LocalDateTime.now());
        this.productDao.save(product2);

        Product product3 = new Product();
        product3.setName("Apple MacBook Air");
        product3.setDescription("13-inch Retina display, 1.6GHz dual-core Intel Core i5, 128GB" +
                " - Silver (Latest Model)");
        product3.setPrice(1079.99f);
        product3.setCategory(new Category(2L));
        product3.setCreated(LocalDateTime.now());
        this.productDao.save(product3);

        Product product4 = new Product();
        product4.setName("ASUS Chromebook C202SA-YS02");
        product4.setDescription("1.6\" Ruggedized and Water Resistant Design with 180 Degree " +
                "(Intel Celeron 4 GB, 16GB eMMC, Dark Blue, Silver)");
        product4.setPrice(212.67f);
        product4.setCategory(new Category(2L));
        product4.setCreated(LocalDateTime.now());
        this.productDao.save(product4);

        Product product5 = new Product();
        product5.setName("ASUS ROG Zephyrus S Ultra Slim Gaming PC Laptop");
        product5.setDescription("15.6” 144Hz IPS Type, Intel Core i7-8750H CPU, GeForce GTX 1070, 16GB DDR4," +
                " 512GB PCIe SSD, Military-Grade Metal Chassis, Win 10 Home - GX531GS-AH76");
        product5.setPrice(1799.00f);
        product5.setCategory(new Category(2L));
        product5.setCreated(LocalDateTime.now());
        this.productDao.save(product5);

        Product product6 = new Product();
        product6.setName("Nintendo Switch");
        product6.setPrice(299.00f);
        product6.setCategory(new Category(3L));
        product6.setCreated(LocalDateTime.now());
        this.productDao.save(product6);

        Product product7 = new Product();
        product7.setName("PlayStation 4 Slim 1TB Console");
        product7.setPrice(299.00f);
        product7.setCategory(new Category(3L));
        product7.setCreated(LocalDateTime.now());
        this.productDao.save(product7);

        Product product8 = new Product();
        product8.setName("Xbox Wireless Controller - White");
        product8.setPrice(59.99f);
        product8.setCategory(new Category(3L));
        product8.setCreated(LocalDateTime.now());
        this.productDao.save(product8);

        Product product9 = new Product();
        product9.setName("Xbox Wireless Controller - Black");
        product9.setPrice(26.99f);
        product9.setCategory(new Category(3L));
        product9.setCreated(LocalDateTime.now().minusMinutes(1));
        this.productDao.save(product9);

        Product product10 = new Product();
        product10.setName("Oculus Go Standalone Virtual Reality Headset - 64GB");
        product10.setPrice(259.67f);
        product10.setCategory(new Category(3L));
        product10.setCreated(LocalDateTime.now());
        this.productDao.save(product10);

        Product product11 = new Product();
        product11.setName("Resident Evil 2 - PlayStation 4");
        product11.setPrice(39.99f);
        product11.setCategory(new Category(3L));
        product11.setCreated(LocalDateTime.now());
        this.productDao.save(product11);

        Product product12 = new Product();
        product12.setName("Cold Waters (Normal, Alabama Book 1) ");
        product12.setDescription("From USA Today bestselling author Debbie Herbert comes a " +
                "thrilling story of murder and madness set in the darkest corner of Alabama.");
        product12.setPrice(4.99f);
        product12.setCategory(new Category(4L));
        product12.setCreated(LocalDateTime.now());
        this.productDao.save(product12);

        Product product13 = new Product();
        product13.setName("Thick");
        product13.setPrice(0.99f);
        product13.setCategory(new Category(4L));
        product13.setCreated(LocalDateTime.now());
        this.productDao.save(product13);

        Product product14 = new Product();
        product14.setName("HUGGIES Snug & Dry Baby Diapers, Size 4");
        product14.setPrice(31.34f);
        product14.setCategory(new Category(5L));
        product14.setCreated(LocalDateTime.now());
        this.productDao.save(product14);

        Product product15 = new Product();
        product15.setName("Linenspa 6 Inch Innerspring Mattress - Twin");
        product15.setPrice(71.80f);
        product15.setCategory(new Category(6L));
        product15.setCreated(LocalDateTime.now());
        this.productDao.save(product15);

        Product product16 = new Product();
        product16.setName("Arozzi Verona V2 Advanced Racing Style Gaming Chair");
        product16.setPrice(271.34f);
        product16.setCategory(new Category(6L));
        product16.setCreated(LocalDateTime.now());
        this.productDao.save(product16);

        Product product17 = new Product();
        product17.setName("LUCID 3-inch Gel Memory Foam Mattress Topper");
        product17.setPrice(99.99f);
        product17.setCategory(new Category(6L));
        product17.setCreated(LocalDateTime.now());
        this.productDao.save(product17);
    }

    /**
     * This method is used to create default categories.
     */
    private void createDefaultCategories() {
        LOG.info("We're creating the default categories!");
        Category smartphones = new Category("Smartphones");
        this.categoryDao.save(smartphones);

        Category technology = new Category("Technology");
        this.categoryDao.save(technology);

        Category videoGames = new Category("Video Games");
        this.categoryDao.save(videoGames);

        Category books = new Category("Books");
        this.categoryDao.save(books);

        Category health = new Category("Health & Household");
        this.categoryDao.save(health);

        Category home = new Category("Home & Kitchen");
        this.categoryDao.save(home);

        LOG.info("The categories has been created!");
    }

}
