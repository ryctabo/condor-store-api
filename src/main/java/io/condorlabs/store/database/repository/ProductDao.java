/*
 * The MIT License
 *
 * Copyright (c) 2019 Gustavo Pacheco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package io.condorlabs.store.database.repository;

import io.condorlabs.store.core.shared.OrderType;
import io.condorlabs.store.database.DataAccessObject;
import io.condorlabs.store.database.entity.Product;

import java.util.List;

/**
 * The <strong>ProductDao</strong> interface provides the methods necessary
 * to management the {@link Product} entity.
 *
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
public interface ProductDao extends DataAccessObject<Product, Long> {

    /**
     * Get a product list from the given parameters.
     *
     * @param search     value to search by name
     * @param categoryId the category ID
     * @param orderBy    property to order
     * @param orderType  order type
     * @param start      index of first element to get
     * @param size       list size
     * @return list of products
     */
    List<Product> find(String search, Long categoryId,
                       String orderBy, OrderType orderType,
                       int start, int size);

    /**
     * Get the amount of items that can be found with the following parameters.
     *
     * @param search     value to search by name
     * @param categoryId the category ID
     * @return amount of elements to get
     */
    long count(String search, Long categoryId);

}
