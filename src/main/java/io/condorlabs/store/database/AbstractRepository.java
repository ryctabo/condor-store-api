/*
 * The MIT License
 *
 * Copyright (c) 2019 Gustavo Pacheco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package io.condorlabs.store.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The <strong>AbstractRepository</strong> class contains all method
 * to manage entities.
 *
 * @param <T> Entity class.
 * @param <I> Data type of primary key or entity's ID.
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
public class AbstractRepository<T extends Serializable, I> implements DataAccessObject<T, I> {

    /**
     * Logger used to record all events in this class.
     */
    private static final Logger LOG = LogManager.getLogger(AbstractRepository.class);

    /**
     * The name of persistence unit of datasource for connection in the database.
     */
    private static final String PERSISTENCE_UNIT = "condorlabs-pu";

    /**
     * The environment variable to replace database user value.
     */
    private static final String ENV_DATABASE_USER = "DB_USER";

    /**
     * The environment variable to replace database password value.
     */
    private static final String ENV_DATABASE_PASSWORD = "DB_PWD";

    /**
     * The environment variable to replace database URL value.
     */
    private static final String ENV_DATABASE_URL = "DB_URL";

    /**
     * Type of class to manage in this class.
     */
    private final Class<T> entityClass;

    /**
     * The entity manager factory created by persistence unit.
     */
    private static EntityManagerFactory emf;

    /**
     * Create a new instance of {@link AbstractRepository} to manage entities.
     *
     * @param entityClass type of class to manage
     */
    public AbstractRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Create entity manager factory from the environment variable and the persistence.xml
     * file.
     *
     * @return entity manager factory
     */
    private static EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
            Map<String, String> configOverrides = new HashMap<>();
            Map<String, String> environments = System.getenv();

            if (environments.containsKey(ENV_DATABASE_USER)) {
                String databaseUser = environments.get(ENV_DATABASE_USER);
                LOG.info("[PERSISTENCE] It's configuring the database user with " +
                        "the environment variable DB_USER = {}", databaseUser);
                configOverrides.put("javax.persistence.jdbc.user", databaseUser);
            }

            if (environments.containsKey(ENV_DATABASE_PASSWORD)) {
                LOG.info("[PERSISTENCE] It's configuring the database password with the " +
                        "environment variable DB_PWD = ****");
                configOverrides.put("javax.persistence.jdbc.password",
                        environments.get(ENV_DATABASE_PASSWORD));
            }

            if (environments.containsKey(ENV_DATABASE_URL)) {
                String databaseUrl = environments.get(ENV_DATABASE_URL);
                LOG.info("[PERSISTENCE] It's configuring the database url with " +
                        "the environment variable DB_URL = {}", databaseUrl);
                configOverrides.put("javax.persistence.jdbc.url", databaseUrl);
            }

            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT, configOverrides);
        }
        return emf;
    }

    /**
     * This method create an entity manager to manage the
     * main operation of an entity.
     *
     * @return entity manager
     */
    protected EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }

    @Override
    public List<T> find() {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaQuery<T> query = entityManager.getCriteriaBuilder()
                    .createQuery(this.entityClass);
            query.select(query.from(this.entityClass));
            return entityManager.createQuery(query).getResultList();
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public T find(I id) {
        if (id == null) return null;
        EntityManager entityManager = getEntityManager();
        try {
            return entityManager.find(this.entityClass, id);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public T save(T entity) {
        if (entity == null) return null;
        EntityManager entityManager = getEntityManager();
        try {
            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            T entitySaved = entityManager.merge(entity);
            transaction.commit();
            return entitySaved;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public T delete(I id) {
        if (id == null) return null;
        EntityManager entityManager = getEntityManager();
        try {
            T entity = entityManager.find(this.entityClass, id);
            if (entity != null) {
                EntityTransaction transaction = entityManager.getTransaction();
                transaction.begin();
                entityManager.remove(entity);
                transaction.commit();
            }
            return entity;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        } finally {
            entityManager.close();
        }
    }

}