/*
 * The MIT License
 *
 * Copyright (c) 2019 Gustavo Pacheco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package io.condorlabs.store.service;

import io.condorlabs.store.core.domain.ListResponse;
import io.condorlabs.store.core.domain.ProductData;
import io.condorlabs.store.core.shared.OrderType;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
public interface ProductService {

    /**
     * Get a product list from the given parameters.
     *
     * @param search     value to search by product name
     * @param categoryId the category ID
     * @param orderBy    property to order
     * @param orderType  order type
     * @param start      index of first element to get
     * @param size       list size
     * @return list of products
     */
    ListResponse<ProductData> get(String search, Long categoryId,
                                  String orderBy, OrderType orderType,
                                  int start, int size);

    /**
     * Get product from the given ID.
     *
     * @param id the product ID
     * @return product data
     */
    ProductData get(Long id);

    /**
     * Create a new product from the given data.
     *
     * @param data the product data to save
     * @return product data saved
     */
    ProductData add(ProductData data);

    /**
     * Update product from the given ID and data.
     *
     * @param id   the product ID
     * @param data product data
     * @return product data updated
     */
    ProductData update(Long id, ProductData data);

    /**
     * Delete a product from the given ID.
     *
     * @param id the product ID
     * @return the product deleted
     */
    ProductData delete(Long id);

}
