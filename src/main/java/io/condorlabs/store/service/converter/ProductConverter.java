/*
 * The MIT License
 *
 * Copyright (c) 2019 Gustavo Pacheco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package io.condorlabs.store.service.converter;

import io.condorlabs.store.core.domain.ProductData;
import io.condorlabs.store.database.entity.Category;
import io.condorlabs.store.database.entity.Product;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ProductConverter implements DtoConverter<Product, ProductData, ProductData> {

    private final CategoryConverter categoryConverter;

    @Inject
    public ProductConverter(CategoryConverter categoryConverter) {
        this.categoryConverter = categoryConverter;
    }

    @Override
    public Product convertToEntity(ProductData data) {
        Product product = new Product();

        product.setName(data.getName());
        product.setDescription(data.getDescription());
        product.setPrice(data.getPrice());

        Category category = new Category(data.getCategory().getId());
        product.setCategory(category);

        return product;
    }

    @Override
    public ProductData convertToDto(Product entity) {
        ProductData product = new ProductData();

        product.setId(entity.getId());
        product.setName(entity.getName());
        product.setDescription(entity.getDescription());
        product.setPrice(entity.getPrice());
        product.setCategory(this.categoryConverter.convertToDto(entity.getCategory()));
        product.setCreated(entity.getCreated());
        product.setUpdated(entity.getUpdated());

        return product;
    }
}
