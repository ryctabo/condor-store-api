/*
 * Copyright (c) 2020 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package io.condorlabs.store.rest.mapper;

import io.condorlabs.store.core.domain.ConstraintData;
import io.condorlabs.store.core.domain.ErrorMessage;

import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
@Provider
public class ConstraintViolationExceptionMapper
        implements ExceptionMapper<ConstraintViolationException> {

    private static final String ERROR_CODE = "CONSTRAINT_VIOLATION";

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(prepareMessage(exception))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    private ErrorMessage prepareMessage(ConstraintViolationException exception) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setErrorCode(ERROR_CODE);

        exception.getConstraintViolations().stream()
                .map(constraint -> {
                    Object invalidValue = constraint.getInvalidValue();
                    Path propertyPath = constraint.getPropertyPath();
                    return new ConstraintData(
                            constraint.getMessage(),
                            propertyPath != null ? propertyPath.toString() : null,
                            invalidValue != null ? invalidValue.toString() : null
                    );
                })
                .forEach(errorMessage::addMessage);

        return errorMessage;
    }

}
