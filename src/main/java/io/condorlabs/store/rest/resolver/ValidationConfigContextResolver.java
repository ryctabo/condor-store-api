/*
 * Copyright (c) 2020 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package io.condorlabs.store.rest.resolver;

import io.condorlabs.store.util.interpolator.LocaleSpecificMessageInterpolator;
import org.glassfish.jersey.server.validation.ValidationConfig;

import javax.validation.MessageInterpolator;
import javax.validation.ParameterNameProvider;
import javax.validation.Validation;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Custom configuration of validation. This configuration can define custom:
 * <ul>
 * <li>MessageInterpolator - interpolates a given constraint violation message.</li>
 * <li>TraversableResolver - determines if a property can be accessed by the Bean Validation provider.</li>
 * <li>ConstraintValidatorFactory - instantiates a ConstraintValidator instance based off its class.
 * <li>ParameterNameProvider - provides names for method and constructor parameters.</li> *
 * </ul>
 *
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
@Provider
public class ValidationConfigContextResolver implements ContextResolver<ValidationConfig> {

    /**
     * Get a context of type {@code GeneralValidator} that is applicable to the supplied type.
     *
     * @param type type the class of object for which a context is desired
     * @return a context for the supplied type or {@code null} if a context
     * for the supplied type is not available from this provider.
     */
    @Override
    public ValidationConfig getContext(Class<?> type) {
        ValidationConfig config = new ValidationConfig();

        MessageInterpolator defaultInterpolator = Validation
                .byDefaultProvider()
                .configure()
                .getDefaultMessageInterpolator();

        config.messageInterpolator(new LocaleSpecificMessageInterpolator(defaultInterpolator));
        config.parameterNameProvider(new CustomParameterNameProvider());

        return config;
    }

    /**
     * If method input parameters are invalid, this class returns actual
     * parameter names instead of the default ones ({@code arg0, arg1, ...})
     */
    private static class CustomParameterNameProvider implements ParameterNameProvider {

        private final ParameterNameProvider nameProvider;

        public CustomParameterNameProvider() {
            this.nameProvider = Validation.byDefaultProvider().configure()
                    .getDefaultParameterNameProvider();
        }

        @Override
        public List<String> getParameterNames(Constructor<?> constructor) {
            return this.nameProvider.getParameterNames(constructor);
        }

        @Override
        public List<String> getParameterNames(Method method) {
            return this.nameProvider.getParameterNames(method);
        }
    }

}
