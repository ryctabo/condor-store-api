/*
 * Copyright (c) 2020 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package io.condorlabs.store.util.interpolator;

import io.condorlabs.store.util.LocaleThreadLocal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.MessageInterpolator;
import java.util.Locale;

/**
 * Delegates to a MessageInterpolator implementation but enforces a given Locale.
 *
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
public class LocaleSpecificMessageInterpolator implements MessageInterpolator {

    private static final Logger LOGGER = LogManager.getLogger(LocaleSpecificMessageInterpolator.class);

    private final MessageInterpolator defaultInterpolator;

    public LocaleSpecificMessageInterpolator(MessageInterpolator interpolator) {
        this.defaultInterpolator = interpolator;
    }

    /**
     * Enforces the locale passed to the interpolator.
     *
     * @param message message template
     * @param context context
     * @return message value
     */
    @Override
    public String interpolate(String message, Context context) {
        LOGGER.debug("Selecting the language {} for the error message.", LocaleThreadLocal.get());
        return defaultInterpolator.interpolate(message, context, LocaleThreadLocal.get());
    }

    @Override
    public String interpolate(String message, Context context, Locale locale) {
        return defaultInterpolator.interpolate(message, context, locale);
    }

}
