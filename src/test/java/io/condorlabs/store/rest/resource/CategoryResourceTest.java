/*
 * The MIT License
 *
 * Copyright (c) 2019 Gustavo Pacheco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package io.condorlabs.store.rest.resource;

import io.condorlabs.store.config.SpringTestConfig;
import io.condorlabs.store.core.domain.CategoryData;
import io.condorlabs.store.generator.CategoryGenerator;
import io.condorlabs.store.service.CategoryService;
import org.glassfish.jersey.uri.internal.JerseyUriBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringTestConfig.class})
public class CategoryResourceTest {

    private CategoryService service;

    private CategoryResource resource;

    private CategoryData data = CategoryGenerator.getData();

    @Autowired
    public void setService(CategoryService service) {
        this.service = service;
    }

    @Before
    public void setUp() {
        this.resource = new CategoryResource(service);
    }

    @Test
    public void testGetAllItems() {
        List<CategoryData> result = this.resource.get();

        assertNotNull(result);

        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testPost() {
        UriInfo uriInfo = mock(UriInfo.class);
        when(uriInfo.getAbsolutePathBuilder()).thenReturn(new JerseyUriBuilder());
        Response response = this.resource.post(this.data, uriInfo);

        assertNotNull(response);

        assertEquals(201, response.getStatus());
    }

    @Test
    public void testDelete() {
        CategoryData result = this.resource.delete(1L);

        assertNotNull(result);
        assertEquals(new Long(1L), result.getId());
    }

}